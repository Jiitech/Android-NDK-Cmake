package com.jonyker.ndktest;

/**
 * 项目名称：NDKTest
 * 创建时间：2018/7/25 0025 下午 12:31
 * 作者：Jonyker
 * 博客：http://blog.csdn.net/jerry_137188
 * github：https://github.com/Jiitech
 * 修改人：Jonyker
 * 联系方式：QQ/534098845
 * 修改时间：2018/7/25 0025 下午 12:31
 * 备注：
 * 版本：V.1.0
 * 描述：
 */
public class MyJNI {

    public native String stringFromJNI();

}
