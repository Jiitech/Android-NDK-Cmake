# Android-make-so

#### 项目介绍
从 Android Studio 2.2 开始，就默认使用 CMake 工具构建 NDK 项目，请确保你的 AS 版本大于 2.2 。



### 1. 通过 IDE 自动构建
- 创建项目时，勾选 Include C++ support 。
- 选择默认的 Toolchain Default 。
- AS 自动生成 CMakeLists.txt 文件（CMake 构建脚本）。


**CMakeLists.txt**
```bash
# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

# 指定CMake的最小版本
cmake_minimum_required(VERSION 3.4.1)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

# 设置模块名字为 native-lib,SHARED 可分享的，以及配置源文件的路径
add_library( # Sets the name of the library.
             native-lib

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             src/main/cpp/native-lib.cpp )

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

# 查找 log 本都模块
find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

# 关联 native-lib 模块和 log 模块
target_link_libraries( # Specifies the target library.
                       native-lib

                       # Links the target library to the log library
                       # included in the NDK.
                       ${log-lib} )
```
**再配置 app/build.gradle ，针对特殊平台 abiFilters 。配置完成之后，同步，运行**
```groovy
android {
    compileSdkVersion 27
    defaultConfig {
        applicationId "com.jonyker.ndktest"
        minSdkVersion 15
        targetSdkVersion 27
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
        externalNativeBuild {
            cmake {
                cppFlags "-frtti -fexceptions"
            }
        }
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
    externalNativeBuild {
        cmake {
            path "CMakeLists.txt"
        }
    }
}
```

### 2. 手动构建

新建一个工程，创建 native 类，快捷键 Alt + Enter ,自动创建 jni 目录和相应的 .cpp 文件。 

- 在工程根目录下创建 CMakeLists.txt 文件。 
- 选择 app modulde ，右击选择Link C++ Project with Gradle。 
- 选择脚本文件的路径。 
- app/build.gradle 会自动同步。同步完成后，运行项目。 

